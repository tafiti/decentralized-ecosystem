# Hello World

Cut to the chase - this document will include one link from each project, of how to take it for a test drive.

Please submit PRs with a one-liner link and short description for your project!

**Ceramic** is a [sovereign data network](https://developers.ceramic.network/learn/welcome/) that lets hundreds of applications (1) create, store and manage dynamic content and metadata (2) on a scalable and fast decentralized network (3) with flexible open source data models and (4) all data controlled and shareable via cross-platform decentralized identities.
